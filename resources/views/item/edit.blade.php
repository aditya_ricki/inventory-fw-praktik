@extends('adminlte::page')

@section('title', 'Edit Item')

@section('content_header')
    <div class="row">
        <div class="col-md-6">
            <h1 class="m-0 text-dark d-inline">Edit item</h1>
            <a href="{{ route('item.index') }}" class="btn btn-secondary float-right"><i class="fas fa-arrow-left"></i> Back</a>

            @if(session('success'))
                <div class="alert alert-success alert-block mt-4">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ session('success') }}</strong>
                </div>
            @endif
        </div>
    </div>
@stop

@section('content')
    <script type="text/javascript">
        function pilihIdRack(obj, rackId)
        {
            $('#rackId').val(rackId);
            var rackId = `${$(obj).parent().parent().find("td:eq(2)").text()}`;
            $("#pilihRack").val(rackId);
            $(".modal").modal('hide');
        }
    </script>
    <div class="modal fade"  id="modalJurusan">
        <div class="modal-dialog">
            <div style="width:700px;" class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Daftar Rack</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="card-body">
                        <table class="table table-striped" id="dataTableRack">
                        <thead>
                            <th>No</th>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach ($racks as $rack)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $rack->code }}</td>
                                    <td>{{ $rack->name }}</td>
                                    <td>
                                        <button data-dismiss="modal" aria-expanded="false" class="btn btn-primary btn-sm" type="button" onclick="pilihIdRack(this, <?php echo $rack->id ?>)">Pilih</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <small>&copy 2020 <a href="http://www.langitinspirasi.co.id">Kelompok 4</a> </small>
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal-Rack -->

    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h5 class="title">Edit Rack</h5>
                </div>
                <form method="post" action="{{ route('item.update', ['item' => $item->id]) }}" autocomplete="off">
                    <div class="card-body">
                        @csrf
                        @method('PUT')

                        <div class="form-group">
                            <label for="rack_id">Rack</label>
                            <input type="text" data-toggle="modal" data-target="#modalJurusan" name="rack_id" class="form-control @error('rack_id') is-invalid @enderror" id="pilihRack" placeholder="Rack" readonly value="{{ $item->rack->name }}">
                            <input type="hidden" name="rack_id" class="form-control" id="rackId" readonly value="{{ $item->rack_id }}">
                            @error('rack_id')
                                <p class="small text-danger">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="form-group{{ $errors->has('code') ? ' has-danger' : '' }}">
                            <label>Code</label>
                            <input type="text" name="code" class="form-control{{ $errors->has('code') ? ' is-invalid' : '' }}" value="{{ $item->code }}" disabled>
                            @error('code')
                                <p class="small text-danger">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Name" value="{{ $item->name }}">
                            @error('name')
                                <p class="small text-danger">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('price') ? ' has-danger' : '' }}">
                                    <label>Price</label>
                                    <input type="number" name="price" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" placeholder="Price" value="{{ $item->price }}">
                                    @error('price')
                                        <p class="small text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('stock') ? ' has-danger' : '' }}">
                                    <label>Stock</label>
                                    <input type="number" name="stock" class="form-control{{ $errors->has('stock') ? ' is-invalid' : '' }}" placeholder="Stock" value="{{ $item->stock }}">
                                    @error('stock')
                                        <p class="small text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-fill btn-primary"><i class="fas fa-edit"></i> Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
