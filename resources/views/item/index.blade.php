@extends('adminlte::page')

@section('title', 'Items Page')

@section('content_header')
    <h1 class="m-0 text-dark d-inline">List of items</h1>
    <a href="{{ route('item.create') }}" class="btn btn-success float-right"><i class="fas fa-plus"></i> Create</a>

    @if(session('success'))
        <div class="alert alert-success alert-block mt-4">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ session('success') }}</strong>
        </div>
    @endif

    @if(session('error'))
        <div class="alert alert-danger alert-block mt-4">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ session('error') }}</strong>
        </div>
    @endif
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table id="dataTableItem" class="table table-striped table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Rack</th>
                                <th>Price</th>
                                <th>Stock</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($items as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->code }}</td>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->rack->name }}</td>
                                <td>@currency($item->price)</td>
                                <td>{{ $item->stock }}</td>
                                <td>
                                    <form action="{{ route('item.destroy', $item->id) }}" method="post" class="d-inline">
                                        <div class="btn-group">
                                            <a href="{{ route('image_item', ['itemId' => $item->id]) }}" class="btn btn-info btn-sm">
                                                <i class="fas fa-image text-white"></i>
                                            </a>
                                            <a href="{{ route('item.edit', ['item' => $item->id]) }}" class="btn btn-warning btn-sm">
                                                <i class="fas fa-edit text-white"></i>
                                            </a>
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-sm btn-danger">
                                                <i class="fas fa-trash text-white"></i>
                                            </button>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
@stop

@section('js')
    <script>
        $(document).ready(() => {
            $('#dataTableItem').DataTable();
        })
    </script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
@stop