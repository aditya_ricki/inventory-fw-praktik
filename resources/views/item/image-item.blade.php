@extends('adminlte::page')

@section('title', 'Image Items Page')

@section('content_header')
    <h1 class="m-0 text-dark d-inline">List of image items</h1>
    <a href="{{ route('item.index') }}" class="btn btn-secondary float-right ml-2"><i class="fas fa-arrow-left"></i> Back</a>
    <a href="#" class="btn btn-success float-right" data-toggle="modal" data-target="#modalCreate"><i class="fas fa-plus"></i> Create</a>

    <div class="modal fade" id="modalCreate" tabindex="-1" role="dialog" aria-labelledby="modalCreateLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{ route('image_item.store', ['itemId' => request()->itemId]) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('POST')
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="imageItem">Image item</label>
                            <input type="file" class="form-control-file{{ $errors->has('image') ? ' is-invalid' : '' }}" id="imageItem" name="image">
                            @error('image')
                                <p class="small text-danger">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Upload</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @if(session('success'))
        <div class="alert alert-success alert-block mt-4">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ session('success') }}</strong>
        </div>
    @endif

    @if(session('error'))
        <div class="alert alert-danger alert-block mt-4">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ session('error') }}</strong>
        </div>
    @endif
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @if(count($imageItems) < 1)
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <p class="text-mute">Image items is empty!</p>
                        </div>
                    </div>
                    @else
                    <div class="row">
                        @foreach($imageItems as $imageItem)
                        <div class="col-sm-2">
                            <form action="{{ route('image_item.destroy', $imageItem->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <img src="{{ Storage::url($imageItem->path) }}" class="img-fluid mb-2" alt="#" title="DELETE THIS IMAGE?"/>
                                <button type="submit" class="btn btn-danger btn-sm btn-block"><i class="fas fa-trash"></i> Delete</button>
                            </form>
                        </div>
                        @endforeach
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="{{ asset('vendor/ekko-lightbox/ekko-lightbox.css') }}">
@stop

@section('js')
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="{{ asset('vendor/ekko-lightbox/ekko-lightbox.min.js') }}"></script>
    <script>
        $(document).ready(() => {
            $(document).on('click', '[data-toggle="lightbox"]', function(event) {
                event.preventDefault()
                $(this).ekkoLightbox({
                    alwaysShowClose: true
                })
            })
        })
    </script>
@stop