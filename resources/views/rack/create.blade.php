@extends('adminlte::page')

@section('title', 'Create Rack')

@section('content_header')
    <div class="row">
        <div class="col-md-6">
            <h1 class="m-0 text-dark d-inline">Create rack</h1>
            <a href="{{ route('rack.index') }}" class="btn btn-secondary float-right"><i class="fas fa-arrow-left"></i> Back</a>

            @if(session('success'))
                <div class="alert alert-success alert-block mt-4">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ session('success') }}</strong>
                </div>
            @endif
        </div>
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <form method="post" action="{{ route('rack.store') }}" autocomplete="off">
                    <div class="card-body">
                        @csrf
                        @method('POST')

                        <div class="form-group{{ $errors->has('code') ? ' has-danger' : '' }}">
                            <label>Code</label>
                            <input type="text" name="code" class="form-control{{ $errors->has('code') ? ' is-invalid' : '' }}" value="{{ Str::upper(Str::random(2)) . date('Ymd') }}">
                            @error('code')
                                <p class="small text-danger">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Name" value="{{ old('name') }}">
                            @error('name')
                                <p class="small text-danger">{{ $message }}</p>
                            @enderror
                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-fill btn-primary"><i class="fas fa-save"></i> Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
