@extends('adminlte::page')

@section('title', 'Edit Incoming Item')

@section('content_header')
    <div class="row">
        <div class="col-md-6">
            <h1 class="m-0 text-dark d-inline">Edit incoming item</h1>
            <a href="{{ route('incoming-item.index') }}" class="btn btn-secondary float-right"><i class="fas fa-arrow-left"></i> Back</a>

            @if(session('success'))
                <div class="alert alert-success alert-block mt-4">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ session('success') }}</strong>
                </div>
            @endif
        </div>
    </div>
@stop

@section('content')
    <script type="text/javascript">
        function pilihIdItem(obj, itemId)
        {
            $('#itemId').val(itemId);
            var itemId = `${$(obj).parent().parent().find("td:eq(2)").text()}`;
            $("#pilihItem").val(itemId);
            $(".modal").modal('hide');
        }
    </script>
    <div class="modal fade" id="modalItems">
        <div class="modal-dialog">
            <div style="width:700px;" class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Daftar Items</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="card-body">
                        <table class="table table-striped" id="dataTableItem">
                        <thead>
                            <th>No</th>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Stock</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach ($items as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->code }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->stock }}</td>
                                    <td>
                                        <button data-dismiss="modal" aria-expanded="false" class="btn btn-primary btn-sm" type="button" onclick="pilihIdItem(this, <?php echo $item->id ?>)">Pilih</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <small>&copy 2020 <a href="http://www.langitinspirasi.co.id">Kelompok 4</a> </small>
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal-Item -->

    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h5 class="title">Edit Rack</h5>
                </div>
                <form method="post" action="{{ route('incoming-item.update', $incomingItem->id) }}" autocomplete="off">
                    <div class="card-body">
                        @csrf
                        @method('PUT')

                        <div class="form-group">
                            <label for="item_id">Item</label>
                            <input type="text" data-toggle="modal" data-target="#modalItems" name="item_id" class="form-control @error('item_id') is-invalid @enderror" id="pilihItem" placeholder="Item" readonly value="{{ $incomingItem->item->name }}">
                            <input type="hidden" name="item_id" class="form-control" id="itemId" readonly value="{{ $incomingItem->item_id }}">
                            @error('item_id')
                                <p class="small text-danger">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('price') ? ' has-danger' : '' }}">
                                    <label>Price</label>
                                    <input type="number" name="price" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" placeholder="Price" value="{{ $incomingItem->price }}">
                                    @error('price')
                                        <p class="small text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('qty') ? ' has-danger' : '' }}">
                                    <label>Quantity</label>
                                    <input type="number" name="qty" class="form-control{{ $errors->has('qty') ? ' is-invalid' : '' }}" placeholder="Quantity" value="{{ $incomingItem->qty }}">
                                    @error('qty')
                                        <p class="small text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-fill btn-primary"><i class="fas fa-edit"></i> Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
