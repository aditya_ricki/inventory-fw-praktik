@extends('adminlte::page')

@section('title', 'Profile')

@section('content_header')
    <h1 class="m-0 text-dark d-inline">Edit profile</h1>
    <a href="{{ route('user.index') }}" class="btn btn-secondary float-right"><i class="fas fa-arrow-left"></i> Back</a>
@stop

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h5 class="title">Edit Profile</h5>
                </div>
                <form method="post" action="{{ route('user.update', ['user' => Auth::user()->id]) }}" autocomplete="off">
                    <div class="card-body">
                            @csrf
                            @method('PUT')

                            <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                <label>Name</label>
                                <input type="text" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Name" value="{{ old('name', auth()->user()->name) }}">
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                <label>Email</label>
                                <input type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email" value="{{ old('email', auth()->user()->email) }}">
                            </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-fill btn-primary"><i class="fas fa-edit"></i> Update</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h5 class="title">Password</h5>
                </div>
                <form method="post" action="#" autocomplete="off">
                    <div class="card-body">
                        @csrf
                        @method('PUT')

                        <div class="form-group{{ $errors->has('old_password') ? ' has-danger' : '' }}">
                            <label>Current password</label>
                            <input type="password" name="old_password" class="form-control{{ $errors->has('old_password') ? ' is-invalid' : '' }}" placeholder="Current password" value="" required disabled>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                            <label>New Password</label>
                            <input type="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="New password" value="" required disabled>
                        </div>
                        <div class="form-group">
                            <label>Confirm new password</label>
                            <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm new password" value="" required disabled>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-fill btn-primary" disabled><i class="fas fa-edit"></i> Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
