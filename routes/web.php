<?php

use Illuminate\Support\Facades\Route;

// import all controller
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RackController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\ImageItemController;
use App\Http\Controllers\IncomingItemController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function() {
	Route::get('home', [HomeController::class, 'index'])->name('home');
	Route::resources([
		'user' => UserController::class,
		'rack' => RackController::class,
		'item' => ItemController::class,
		'incoming-item' => IncomingItemController::class,
	]);
	Route::get('image-item/{itemId}', [ImageItemController::class, 'getImageItemsByItem'])->name('image_item');
	Route::post('image-item/{itemId}', [ImageItemController::class, 'storeImageItemsByItem'])->name('image_item.store');
	Route::delete('image-item/{itemId}', [ImageItemController::class, 'deleteImageItemsById'])->name('image_item.destroy');
});
