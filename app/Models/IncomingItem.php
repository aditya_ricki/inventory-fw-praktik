<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class IncomingItem extends Model
{
    use HasFactory;

    protected $fillable = [
    	'item_id',
    	'qty',
    	'price',
    ];

    public function item()
    {
    	return $this->belongsTo(Item::class);
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->toFormattedDateString();
    }
}
