<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    protected $fillable = [
    	'rack_id',
    	'code',
    	'name',
    	'price',
    	'stock',
    ];

    public function rack()
    {
    	return $this->belongsTo(Rack::class);
    }

    public function incomingItems()
    {
        return $this->hasMany(IncomingItem::class);
    }
}
