<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Rack extends Model
{
    use HasFactory;

    protected $fillable = [
    	'code',
    	'name',
    ];

    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->toFormattedDateString();
    }

    public function items()
    {
    	return $this->hasMany(Item::class);
    }
}
