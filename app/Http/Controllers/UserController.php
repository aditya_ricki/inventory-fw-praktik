<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Hash;

class UserController extends Controller
{
    // index
    public function index()
    {
    	$data = User::all();

    	return view('user.index', ['users' => $data]);
    }

    // create
    public function create()
    {
    	return view('user.create');
    }

    // store
    public function store(Request $request)
    {
    	$request->validate([
			'name'     => 'required',
			'email'    => 'required|unique:users|email',
			'password' => 'required|min:6',
    	]);

    	User::create([
			'name'     => $request->name,
			'email'    => $request->email,
			'password' => Hash::make($request->password),
    	]);

    	return redirect()->back()->with('success', 'User created!');
    }

    // edit
    public function edit($id)
    {
    	return view('user.edit');
    }
}
