<?php

namespace App\Http\Controllers;

use App\Models\ImageItem;
use Illuminate\Http\Request;
use Storage;

class ImageItemController extends Controller
{
    // get image items by item
    public function getImageItemsByItem($itemId)
    {
        $imageItems = ImageItem::all();

        return view('item.image-item', ['imageItems' => $imageItems]);
    }

    // store image items by item
    public function storeImageItemsByItem(Request $request, $itemId)
    {
        $request->validate([
            'image' => 'required|file|max:2000',
        ]);

        $path = Storage::putFile(
                    'public/images',
                    $request->file('image'),
                );

        ImageItem::create([
            'item_id' => $itemId,
            'path'    => $path,
        ]);

        return redirect()->back()->with('success', 'Image item uploaded!');
    }

    public function deleteImageItemsById($imageItem)
    {
        $file = ImageItem::find($imageItem);

        Storage::delete($file->path);
        $file->delete();

        return redirect()->back()->with('success', 'Image item removed!');
    }
}
