<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\Rack;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::all();

        return view('item.index', ['items' => $items]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $racks = Rack::all();

        return view('item.create', ['racks' => $racks]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'rack_id' => 'required',
            'code'    => 'required|unique:items|min:10|max:10',
            'name'    => 'required',
            'price'   => 'required',
            'stock'   => 'required',
        ]);

        Item::create($request->all());

        return redirect()->back()->with('success', 'Item created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        $racks = Rack::all();

        return view('item.edit', ['item' => $item, 'racks' => $racks]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        $request->validate([
            'rack_id' => 'required',
            'name'    => 'required',
            'price'   => 'required',
            'stock'   => 'required',
        ]);

        $item->update($request->except(['code']));

        return redirect()->back()->with('success', 'Item updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        // if (count($rack->items) > 0) {
        //     return redirect()->back()->with('error', "Can't remove this shelf. This shelf has stuff!");
        // }

        $item->delete();
        return redirect()->back()->with('success', 'Item successfully removed!');
    }
}
