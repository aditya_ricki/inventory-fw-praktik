<?php

namespace App\Http\Controllers;

use App\Models\IncomingItem;
use Illuminate\Http\Request;
use App\Models\Item;

class IncomingItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $incomingItems = IncomingItem::all();

        return view('incoming-item.index', ['incomingItems' => $incomingItems]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $items = Item::all();

        return view('incoming-item.create', ['items' => $items]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'item_id' => 'required',
            'price'   => 'required',
            'qty'     => 'required',
        ]);

        try {
            $item = Item::find($request->item_id);

            $item->update([
                'stock' => $item->stock + $request->qty,
            ]);

            IncomingItem::create($request->all());

            return redirect()->back()->with('success', 'Incoming item created!');
        } catch (Exception $e) {
            return redirect()->back()->with('error', $e->message);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\IncomingItem  $incomingItem
     * @return \Illuminate\Http\Response
     */
    public function show(IncomingItem $incomingItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\IncomingItem  $incomingItem
     * @return \Illuminate\Http\Response
     */
    public function edit(IncomingItem $incomingItem)
    {
        $items = Item::all();

        return view('incoming-item.edit', ['items' => $items, 'incomingItem' => $incomingItem]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\IncomingItem  $incomingItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IncomingItem $incomingItem)
    {
        $request->validate([
            'item_id' => 'required',
            'price'   => 'required|min:1',
            'qty'     => 'required|min:1',
        ]);

        try {
            $item = Item::find($request->item_id);

            $item->update([
                'stock' => $item->stock - $incomingItem->qty + $request->qty,
            ]);

            $incomingItem->update($request->all());

            return redirect()->back()->with('success', 'Incoming item updated!');
        } catch (Exception $e) {
            return redirect()->back()->with('error', $e->message);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\IncomingItem  $incomingItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(IncomingItem $incomingItem)
    {
        try {
            $item = Item::find($incomingItem->item_id);

            $item->update([
                'stock' => $item->stock - $incomingItem->qty,
            ]);

            $incomingItem->delete();

            return redirect()->back()->with('success', 'Incoming item removed!');
        } catch (Exception $e) {
            return redirect()->back()->with('error', $e->message);
        }
    }
}
